import React, { useState } from 'react'
import { Layout } from '../../components/layouts'
import { NoFavorites } from '../../components/ui'
import { useEffect } from 'react';
import { localFavorites } from '../../utils';
import { FavoritePokemons } from '../../components/pokemon';
import { NextPage } from 'next';

const FavoritePage: NextPage = () => {

  const [favoritePokemons, setFavoritePokemons] = useState<number[]>([]);

  useEffect(() => {
    setFavoritePokemons(localFavorites.pokemons());
  }, []);
  

  return (
    <Layout title='Pokemons -Favorites'>
        {
          favoritePokemons.length === 0 
          ? (<NoFavorites />)
          : (
            <FavoritePokemons pokemons={favoritePokemons} />
          )
        }
    </Layout>
  )
}

export default FavoritePage
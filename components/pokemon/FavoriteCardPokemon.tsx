import { Card, Grid } from '@nextui-org/react'
import { useRouter } from 'next/router';
import React, { FC } from 'react'

interface Props {
    idPokemon: number;
}
export const FavoriteCardPokemon: FC<Props> = ({idPokemon}) => {

  const router = useRouter();

  const onFavoriteClick = () => {
    router.push(`/pokemon/${idPokemon}`);
  }

  return (
    <Grid xs={6} sm={3} md={2} lg={1} key={idPokemon}>
        <Card isHoverable isPressable 
          css={{padding: 10}} 
          onPress={onFavoriteClick}
        >
        <Card.Image 
            src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${idPokemon}.svg`}
            width={'100%'}
            height={140}
        />
        </Card>
    </Grid>
  )
}

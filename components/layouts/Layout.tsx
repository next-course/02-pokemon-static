import Head from 'next/head'
import React, { FC, useEffect } from 'react'
import { Navbar } from '../ui/Navbar';
import {useState} from 'react';

interface Props {
    children?: JSX.Element,
    title?: string,
}

export const Layout: FC<Props> = ({children, title}) => {

  const [originUrl, setoriginUrl] = useState('');

  useEffect(() => {
    setoriginUrl(window.location.origin);
  }, [])

  return (
    <>
        <Head>
            <title>{title || 'PokemonApp'}</title>
            <meta name='author' content='Christian Dionisio' />
            <meta name='description' content={`Información sobre el pokemon ${title}`} />
            <meta name='keywords' content={`${title}, pokemon, pokedex`} />
            <meta property="og:title" content={`Información sobre ${title}`} />
            <meta property="og:description" content={`Esta es la página sobre ${title}`} />
            <meta property="og:image" content={`${originUrl}/img/banner.png`} />
        </Head>

        <Navbar />

        <main style={{
          padding: '0px 20px',
        }}>
            { children }
        </main>
    </>
  )
}
